package synchronizer;

import java.io.IOException;

import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.annotation.Backoff;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;

import synchronizer.entities.BpmnTask;
import synchronizer.issuetracker.IssueTracker;
import synchronizer.issuetracker.JiraIssueTracker;
import synchronizer.issuetracker.RedmineIssueTracker;


@Component
public class IssueTrackerDispatcher
{
  private static final Logger logger = LoggerFactory.getLogger(IssueTrackerDispatcher.class);
  
  @Autowired
  private RedmineIssueTracker redmine;

  @Autowired
  private JiraIssueTracker jira;
  
  @Autowired
  private TaskProcessingNotifier notifier;

  @Retryable(value = { RestClientException.class }, maxAttempts = 5, backoff = @Backoff(delay = 5000))
  public Issue createBpmnTaskIssue(ExternalTask task, ExternalTaskService service) throws InterruptedException, IOException {
  IssueTracker issueTracker = getIssueTracker(task);
  return issueTracker.createBpmnTaskIssue(task);
  }
	
  @Recover
  public Issue recover(RestClientException t, ExternalTask externalTask, ExternalTaskService service)
  throws JsonProcessingException, InterruptedException {
  BpmnTask task = BpmnTask.fromExternalTask(externalTask);
  logger.warn("Issue tracker is unreachable!");
	notifier.notifyFailure(task, "Issue tracker");
	
	// TODO handle BPMN error within the process
	service.handleBpmnError(externalTask,
			"Error in issue tracker's synchronization web service.");
	throw t;
   }
  
  private IssueTracker getIssueTracker(ExternalTask task)
  {
    int issueTrackerId = 1;
    if(task.getVariable("preDefinedIssueTracker") != null)
    {
      issueTrackerId = Integer.parseInt(task.getVariable("preDefinedIssueTracker").toString());
    }
    else if(task.getVariable("issueTracker") != null)
    {
      issueTrackerId = Integer.parseInt(task.getVariable("issueTracker").toString());
    }

    IssueTrackers t = IssueTrackers.get(issueTrackerId);
    switch(t)
    {
      case REDMINE:
      {
        return redmine;
      }
      default:
      {
        return jira;
      }
    }
  }
}
