package synchronizer;

import java.io.IOException;
import java.text.MessageFormat;

import org.camunda.bpm.client.ExternalTaskClient;
import org.camunda.bpm.client.ExternalTaskClientBuilder;
import org.camunda.bpm.client.topic.TopicSubscriptionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.retry.annotation.EnableRetry;
import synchronizer.entities.ExternalReference;

@EnableRetry
@SpringBootApplication
public class WebService
{
  @Autowired
  private ExternalReferenceRepository repository;

  @Autowired
  private IssueTrackerDispatcher issueTracker;
  
  private static final Logger logger = LoggerFactory.getLogger(WebService.class);


  public static void main(String... args) throws InterruptedException
  {
    SpringApplication.run(WebService.class, args);
  }


  @EventListener(ApplicationReadyEvent.class)
  public void runTheWorker()
  {
    String synchronizerTaskWorkerId = "synchronizerTaskWorkerId";
    ExternalTaskClientBuilder clientBuilder = ExternalTaskClient.create();
    clientBuilder.baseUrl("http://localhost:8080/engine-rest/");
    clientBuilder.workerId(synchronizerTaskWorkerId);
    clientBuilder.asyncResponseTimeout(1000);
    ExternalTaskClient client = clientBuilder.build();
    // TODO introduce BackOff strategy

    // subscribe to the topic
    TopicSubscriptionBuilder subscriptionBuilder = client.subscribe("synchronizedTask");
    subscriptionBuilder.handler((externalTask, externalTaskService) -> {
      logger.info(MessageFormat.format("Activity with id {0} fetched and locked by external worker", externalTask.getActivityId()));
      String executionId = externalTask.getExecutionId();
      ExternalReference ref = repository.findByExecutionId(executionId);
      if(ref != null)
      {
        // stop the worker if issue is already sent to Issue Tracker
        return;
      }
      Issue issue;
      try
      {
        issue = issueTracker.createBpmnTaskIssue(externalTask, externalTaskService);
      }
      catch(IOException | InterruptedException ex)
      {

        externalTaskService.handleBpmnError(externalTask, "Error in issue tracker's synchronization web service.");
        return;
      }
      logger.info(MessageFormat.format("Issue with ID: ''{0}'' has been created!", issue.getIdentifier()));
      ExternalReference r = new ExternalReference();
      r.setIssueId(issue.getIdentifier());
      r.setExecutionId(externalTask.getExecutionId());
      r.setTaskId(externalTask.getId());
      r.setTaskWorkerId(synchronizerTaskWorkerId);
      repository.save(r);

    });
    subscriptionBuilder.open();
  }

}
