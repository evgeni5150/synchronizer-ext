package synchronizer;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;


public final class RestClient
{


  private static final Logger logger = LoggerFactory.getLogger(RestClient.class);


  private RestClient()
  {

  }


  public static String postJson(String jsonBody, URI url) throws JsonProcessingException, InterruptedException
  {
    logger.info("Attempting to send data to " + url.toString());
    String result = "";
    final RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.set("Accept", "application/json");
    headers.set("content-type", "application/json;charset=utf-8");
    HttpEntity<String> entity = new HttpEntity<String>(jsonBody, headers);
    result = restTemplate.postForObject(url, entity, String.class);
    logger.info("Data sent succefully!");
    return result;
  }

}
