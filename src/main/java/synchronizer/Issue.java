package synchronizer;


public interface Issue
{
  String getIdentifier();


  String getStatusName();


  boolean isResolved();
}
