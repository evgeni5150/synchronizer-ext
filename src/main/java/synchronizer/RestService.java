package synchronizer;

import java.io.IOException;
import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import synchronizer.entities.BpmnTask;
import synchronizer.entities.ExternalReference;


@RestController
public class RestService
{

  private static final Logger logger = LoggerFactory.getLogger(RestService.class);

  @Autowired
  TaskProcessingNotifier notifier;

  @Autowired
  CamundaApiClient camunda;

  @Autowired
  private ExternalReferenceRepository repository;


  @RequestMapping(value = "/redmine", method = RequestMethod.POST, produces = "application/json")
  public void process(@RequestBody String payload) throws Exception
  {
    IssueMapper mapper = new IssueMapper();
    Issue issue = mapper.readValue(payload);
    String id = issue.getIdentifier();
    String status = issue.getStatusName();
    logger.info(MessageFormat.format("Issue with id: {0} current status is: {1}!", id, status));
    ExternalReference ref = repository.findByIssueId(id);
    if(ref==null || status.equals("New") || status.equals("In Progress")) {
    	return;
    }
    String taskId = ref.getTaskId();
    String workerId = ref.getTaskWorkerId();
    // Send the callback to the process engine.

    completeBpmnTask(taskId, workerId, payload, status);
    logger.info(MessageFormat.format("BPMN Task with id: {0} has been completed!", taskId));
    // remove reference entry from database because it's no longer needed
    repository.delete(ref);
  }


  @Retryable(value = { RestClientException.class }, maxAttempts = 5, backoff = @Backoff(delay = 5000))
  void completeBpmnTask(String taskId, String workerId, String issueJson, String status) throws JsonProcessingException, InterruptedException
  {
    camunda.completeExternalTask(taskId, workerId, status);
  }


  @Recover
  public String recover(RestClientException t, String taskId, String workerId, String issueJSON, String status) throws JsonParseException, JsonMappingException, IOException, InterruptedException
  {
    // TODO Notify BPMN
    ObjectMapper mapper = new ObjectMapper();
    BpmnTask task = mapper.readValue(issueJSON, BpmnTask.class);
    logger.warn("BPMN engine is unreachable. Issue will be saved to database!");
    notifier.notifyFailure(task, "BPMN engine");
    return "";
  }


}
