package synchronizer;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

import synchronizer.entities.BpmnTask;
import synchronizer.messenger.MailMessenger;
import synchronizer.messenger.TelegramMessenger;


@Component
public class TaskProcessingNotifier
{
  @Value("${MAIL_RECEIVER}")
  private String MAIL_RECEIVER;

  @Value("${TELEGRAM_CHAT_ID}")
  private String TELEGRAM_CHAT_ID;


  @Autowired
  private TelegramMessenger telegram;


  @Autowired
  private MailMessenger mail;


  public void notifyFailure(BpmnTask task, String failureSource) throws JsonProcessingException, InterruptedException
  {
    String template = "";
    template += "Issue with subject: {0} and descripton: {1}, for user with id: {2} with Due Date set to: {3} and launched by";
    template += "process with id: {4} has not been sent to {5} due to connectivity problems after 4 attempts.";
    String text = MessageFormat.format(template, task.getSubject(), task.getDescription(), task.getAssigneeId(), task.getDueDate(), task.getProcessId());
    String subject = "Failed to send issue to " + failureSource;

    telegram.send(TELEGRAM_CHAT_ID, subject, text);
    mail.send(MAIL_RECEIVER, subject, text);
    // TODO send notification to Pidgin
  }
}
