package synchronizer;

import org.springframework.data.mongodb.repository.MongoRepository;

import synchronizer.entities.ExternalReference;


public interface ExternalReferenceRepository extends MongoRepository<ExternalReference, String>
{

  public ExternalReference findByIssueId(String issueId);


  public ExternalReference findByTaskId(String taskId);


  public ExternalReference findByExecutionId(String executionId);
}
