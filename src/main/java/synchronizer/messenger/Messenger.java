package synchronizer.messenger;

import com.fasterxml.jackson.core.JsonProcessingException;


public interface Messenger
{
  String send(String to, String subject, String text) throws JsonProcessingException, InterruptedException;
}
