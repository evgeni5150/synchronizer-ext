package synchronizer.messenger;

import java.net.URI;
import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;

import synchronizer.RestClient;


@Component
public class TelegramMessenger implements Messenger
{
  @Value("${TELEGRAM_API_TOKEN}")
  private String TELEGRAM_API_TOKEN;

  private static final Logger logger = LoggerFactory.getLogger(TelegramMessenger.class);


  @Override
  public String send(String to, String subject, String text) throws JsonProcessingException, InterruptedException
  {
    logger.info("Attempting to send message to Telegram");
    String url = MessageFormat.format("https://api.telegram.org/bot{0}/sendMessage", TELEGRAM_API_TOKEN);
    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url).queryParam("chat_id", to).queryParam("text", text);
    URI uri = builder.build().encode().toUri();
    String result = RestClient.postJson(text, uri);
    logger.info("Telegram bot responded with: " + result);
    return result;

  }

}
