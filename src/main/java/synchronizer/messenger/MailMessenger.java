package synchronizer.messenger;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;


@Component
public class MailMessenger implements Messenger
{
  @Autowired
  JavaMailSender javaMailSender;

  private static final Logger logger = LoggerFactory.getLogger(MailMessenger.class);


  @Override
  public String send(String to, String subject, String text)
  {
    // sender and receiver identifications should be specified in
    // application.properties
    if(StringUtils.isEmpty(to))
    {
      logger.warn("MAIL_RECEIVER configuration is empty.");
      return "";
    }

    SimpleMailMessage mail = new SimpleMailMessage();

    mail.setTo(to);
    mail.setSubject(subject);
    mail.setText(text);

    logger.info("Sending mail...");

    javaMailSender.send(mail);

    logger.info("Done!");
    return "";

  }

}
