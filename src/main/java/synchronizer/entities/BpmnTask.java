package synchronizer.entities;

import java.io.Serializable;

import org.camunda.bpm.client.task.ExternalTask;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeName;


@JsonTypeName("issue")
@JsonTypeInfo(include = As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class BpmnTask implements Serializable
{

  private static final long serialVersionUID = 1L;

  @Id
  public String id;

  @JsonProperty("project_id")
  private int projectId;

  private String subject;

  @JsonProperty("assigned_to_id")
  private int assigneeId;


  private String description;

  @JsonIgnore
  private String processId;

  @JsonIgnore
  private int trackerId;

  @JsonProperty("due_date")
  private String dueDate;


  public String getId()
  {
    return id;
  }


  public void setId(String id)
  {
    this.id = id;
  }


  public int getProjectId()
  {
    return projectId;
  }


  public void setProjectId(int projectId)
  {
    this.projectId = projectId;
  }


  public String getSubject()
  {
    return subject;
  }


  public void setSubject(String subject)
  {
    this.subject = subject;
  }


  public int getAssigneeId()
  {
    return assigneeId;
  }


  public void setAssigneeId(int assigneeId)
  {
    this.assigneeId = assigneeId;
  }


  public String getDescription()
  {
    return description;
  }


  public void setDescription(String description)
  {
    this.description = description;
  }


  public String getProcessId()
  {
    return processId;
  }


  public void setProcessId(String processId)
  {
    this.processId = processId;
  }


  public String getDueDate()
  {
    return dueDate;
  }


  public void setDueDate(String dueDate)
  {
    this.dueDate = dueDate;
  }


  public int getTrackerId()
  {
    return trackerId;
  }


  public void setTrackerId(int trackerId)
  {
    this.trackerId = trackerId;
  }


  public static BpmnTask fromExternalTask(ExternalTask externalTask)
  {
    BpmnTask taskIssue = new BpmnTask();
    String processDefinitionKey = externalTask.getProcessDefinitionKey();
    taskIssue.setProjectId(getVariableOrDefault(externalTask, "projectId", 1));
    taskIssue.setSubject(getVariableOrDefault(externalTask, "subject", "no subject"));
    taskIssue.setDescription(generateDescriptionWithDiagram(getVariableOrDefault(externalTask, "description", ""), processDefinitionKey));
    taskIssue.setAssigneeId(getVariableOrDefault(externalTask, "assigneeId", 1));
    taskIssue.setProcessId(externalTask.getId());
    taskIssue.setDueDate(getVariableOrDefault(externalTask, "dueDate", ""));
    return taskIssue;
  }


  private static <T> T getVariableOrDefault(ExternalTask externalTask, String name, T defaultValue)
  {
    String preDefinedName = "preDefined" + name.substring(0, 1).toUpperCase() + name.substring(1);
    // T preDefinedValue = externalTask.getVariable(preDefinedName);
    Object value = externalTask.getVariable(preDefinedName);
    if(value == null)
    {
      value = externalTask.getVariable(name);
    }

    if(value == null)
    {
      return defaultValue;
    }
    if(value.getClass() == String.class)
    {
      if(defaultValue != null)
      {
        String s = (String)value;
        Class<?> defaultValueClass = defaultValue.getClass();
        if(defaultValueClass.equals(Boolean.class))
        {
          value = Boolean.parseBoolean(s);
        }
        else if(defaultValueClass == Short.class)
        {
          value = Short.parseShort(s);
        }
        else if(defaultValueClass == Long.class)
        {
          value = Long.parseLong(s);
        }
        else if(defaultValueClass == Double.class)
        {
          value = Double.parseDouble(s);
        }
        else if(defaultValueClass == Integer.class)
        {
          value = Integer.parseInt(s);
        }
      }
    }
    return (T)value;
  }


  private static String generateDescriptionWithDiagram(String description, String processDefinitionKey)
  {
    return description += "&nbsp; &nbsp; \"Diagram reference\": http://localhost:8080/engine-rest/process-definition/key/" + processDefinitionKey + "/diagram";

  }
}

