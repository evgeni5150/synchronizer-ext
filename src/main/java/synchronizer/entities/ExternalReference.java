package synchronizer.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class ExternalReference
{
  @Id
  private String id;

  public String executionId;
  public String taskId;
  public String taskWorkerId;


  public String getTaskWorkerId()
  {
    return taskWorkerId;
  }


  public void setTaskWorkerId(String taskWorkerId)
  {
    this.taskWorkerId = taskWorkerId;
  }


  public String getTaskId()
  {
    return taskId;
  }


  public void setTaskId(String taskId)
  {
    this.taskId = taskId;
  }


  public String issueId;


  public String getId()
  {
    return id;
  }


  public ExternalReference()
  {
  }


  public void setId(String id)
  {
    this.id = id;
  }


  public String getExecutionId()
  {
    return executionId;
  }


  public void setExecutionId(String executionId)
  {
    this.executionId = executionId;
  }


  public String getIssueId()
  {
    return issueId;
  }


  public void setIssueId(String issueId)
  {
    this.issueId = issueId;
  }

}
