package synchronizer.entities.redmine;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Detail {

@JsonProperty("id")
private Integer id;
@JsonProperty("value")
private String value;
@JsonProperty("old_value")
private String oldValue;
@JsonProperty("prop_key")
private String propKey;
@JsonProperty("property")
private String property;

@JsonProperty("id")
public Integer getId() {
return id;
}

@JsonProperty("id")
public void setId(Integer id) {
this.id = id;
}

@JsonProperty("value")
public String getValue() {
return value;
}

@JsonProperty("value")
public void setValue(String value) {
this.value = value;
}

@JsonProperty("old_value")
public String getOldValue() {
return oldValue;
}

@JsonProperty("old_value")
public void setOldValue(String oldValue) {
this.oldValue = oldValue;
}

@JsonProperty("prop_key")
public String getPropKey() {
return propKey;
}

@JsonProperty("prop_key")
public void setPropKey(String propKey) {
this.propKey = propKey;
}

@JsonProperty("property")
public String getProperty() {
return property;
}

@JsonProperty("property")
public void setProperty(String property) {
this.property = property;
}


}