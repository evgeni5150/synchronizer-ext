package synchronizer.entities.redmine;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import synchronizer.Issue;


@JsonTypeName("issue")
public class RedmineIssue implements Issue
{

  @JsonProperty("id")
  private long id;
  @JsonProperty("project")
  private Project project;
  private Tracker tracker;
  @JsonProperty("status")
  private Status status;
  @JsonProperty("priority")
  private Priority priority;
  @JsonProperty("author")
  private Author author;
  @JsonProperty("subject")
  private String subject;
  @JsonProperty("description")
  private String description;
  @JsonProperty("start_date")
  private String startDate;
  @JsonProperty("done_ratio")
  private Integer doneRatio;
  @JsonProperty("created_on")
  private String createdOn;
  @JsonProperty("updated_on")
  private String updatedOn;
  @JsonProperty("notes")
  private Notes notes;
  @JsonProperty("due_date")
  private String dueDate;
  @JsonProperty("assigned_to")
  private User assignedTo;
  @JsonProperty("watchers")
  private List<Assignee> watchers;

  public List<Assignee> getWatchers() {
	return watchers;
}


public void setWatchers(List<Assignee> watchers) {
	this.watchers = watchers;
}



public User getAssignedTo() {
	return assignedTo;
}


public void setAssignedTo(User assignedTo) {
	this.assignedTo = assignedTo;
}


public Assignee getAssignee() {
	return assignee;
}


public void setAssignee(Assignee assignee) {
	this.assignee = assignee;
}


@JsonProperty("closed_on")
  private String closedOn;
  @JsonProperty("parent_id")
  private String parentId;
  @JsonProperty("root_id")
  private String rootId;
  @JsonProperty("is_private")
  private String isPrivate;
  @JsonProperty("lock_version")
  private String lockVersion;
  @JsonProperty("assignee")
  private Assignee assignee;


public String getLockVersion() {
	return lockVersion;
}


public void setLockVersion(String lockVersion) {
	this.lockVersion = lockVersion;
}


public String getIsPrivate() {
	return isPrivate;
}


public void setIsPrivate(String isPrivate) {
	this.isPrivate = isPrivate;
}


public String getParentId() {
	return parentId;
}


public void setParentId(String parentId) {
	this.parentId = parentId;
}


public String getEstimatedHours() {
	return estimatedHours;
}


public void setEstimatedHours(String estimatedHours) {
	this.estimatedHours = estimatedHours;
}


@JsonProperty("estimated_hours")
  private String estimatedHours;
  

  public String getRootId() {
	return rootId;
}


public void setRootId(String rootId) {
	this.rootId = rootId;
}


public String getClosedOn() {
	return closedOn;
}


public void setClosedOn(String closedOn) {
	this.closedOn = closedOn;
}


  public String getDueDate()
  {
    return dueDate;
  }


  public void setDueDate(String dueDate)
  {
    this.dueDate = dueDate;
  }


  @JsonProperty("notes")
  public Notes getNotes()
  {
    return notes;
  }


  @JsonProperty("notes")
  public void setNotes(Notes notes)
  {
    this.notes = notes;
  }


  @JsonProperty("id")
  public long getId()
  {
    return id;
  }


  @JsonProperty("id")
  public void setId(long id)
  {
    this.id = id;
  }


  @JsonProperty("project")
  public Project getProject()
  {
    return project;
  }


  @JsonProperty("project")
  public void setProject(Project project)
  {
    this.project = project;
  }


  @JsonProperty("tracker")
  public Tracker getTracker()
  {
    return tracker;
  }


  @JsonProperty("tracker")
  public void setTracker(Tracker tracker)
  {
    this.tracker = tracker;
  }


  @JsonProperty("status")
  public Status getStatus()
  {
    return status;
  }


  @JsonProperty("status")
  public void setStatus(Status status)
  {
    this.status = status;
  }


  @JsonProperty("priority")
  public Priority getPriority()
  {
    return priority;
  }


  @JsonProperty("priority")
  public void setPriority(Priority priority)
  {
    this.priority = priority;
  }


  @JsonProperty("author")
  public Author getAuthor()
  {
    return author;
  }


  @JsonProperty("author")
  public void setAuthor(Author author)
  {
    this.author = author;
  }


  @JsonProperty("subject")
  public String getSubject()
  {
    return subject;
  }


  @JsonProperty("subject")
  public void setSubject(String subject)
  {
    this.subject = subject;
  }


  @JsonProperty("description")
  public String getDescription()
  {
    return description;
  }


  @JsonProperty("description")
  public void setDescription(String description)
  {
    this.description = description;
  }


  @JsonProperty("start_date")
  public String getStartDate()
  {
    return startDate;
  }


  @JsonProperty("start_date")
  public void setStartDate(String startDate)
  {
    this.startDate = startDate;
  }


  @JsonProperty("done_ratio")
  public Integer getDoneRatio()
  {
    return doneRatio;
  }


  @JsonProperty("done_ratio")
  public void setDoneRatio(Integer doneRatio)
  {
    this.doneRatio = doneRatio;
  }


  @JsonProperty("created_on")
  public String getCreatedOn()
  {
    return createdOn;
  }


  @JsonProperty("created_on")
  public void setCreatedOn(String createdOn)
  {
    this.createdOn = createdOn;
  }


  @JsonProperty("updated_on")
  public String getUpdatedOn()
  {
    return updatedOn;
  }


  @JsonProperty("updated_on")
  public void setUpdatedOn(String updatedOn)
  {
    this.updatedOn = updatedOn;
  }


  @Override
  public String getIdentifier()
  {
    return Long.toString(id);
  }


  @Override
  public boolean isResolved()
  {
    return getStatus().getName().equals("Resolved");
  }


  @Override
  public String getStatusName()
  {
    return getStatus().getName();
  }

}
