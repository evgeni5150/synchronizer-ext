package synchronizer.entities.redmine;

import java.io.Serializable;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeName;


@JsonTypeName("issue")
@JsonTypeInfo(include = As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class RedmineBpmnTask implements Serializable
{

  private static final long serialVersionUID = 1L;

  @Id
  public String id;

  @JsonProperty("project_id")
  private int projectId;

  private String subject;

  @JsonProperty("assigned_to_id")
  private int assigneeId;

  private String description;

  private String processId;

  private int trackerId;

  @JsonProperty("due_date")
  private String dueDate;


  public String getId()
  {
    return id;
  }


  public void setId(String id)
  {
    this.id = id;
  }


  public int getProjectId()
  {
    return projectId;
  }


  public void setProjectId(int projectId)
  {
    this.projectId = projectId;
  }


  public String getSubject()
  {
    return subject;
  }


  public void setSubject(String subject)
  {
    this.subject = subject;
  }


  public int getAssigneeId()
  {
    return assigneeId;
  }


  public void setAssigneeId(int assigneeId)
  {
    this.assigneeId = assigneeId;
  }


  public String getDescription()
  {
    return description;
  }


  public void setDescription(String description)
  {
    this.description = description;
  }


  public String getProcessId()
  {
    return processId;
  }


  public void setProcessId(String processId)
  {
    this.processId = processId;
  }


  public String getDueDate()
  {
    return dueDate;
  }


  public void setDueDate(String dueDate)
  {
    this.dueDate = dueDate;
  }


  public int getTrackerId()
  {
    return trackerId;
  }


  public void setTrackerId(int trackerId)
  {
    this.trackerId = trackerId;
  }

}
