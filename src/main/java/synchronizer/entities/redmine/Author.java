package synchronizer.entities.redmine;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Author {
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("icon_url")
	private String icon_url;
	@JsonProperty("identity_url")
	private String identity_url;
	@JsonProperty("lastname")
	private String lastname;
	@JsonProperty("firstname")
	private String firstname;
	@JsonProperty("mail")
	private String mail;
	@JsonProperty("login")
	private String login;

	public String getIcon_url() {
		return icon_url;
	}

	public void setIcon_url(String icon_url) {
		this.icon_url = icon_url;
	}

	public String getIdentity_url() {
		return identity_url;
	}

	public void setIdentity_url(String identity_url) {
		this.identity_url = identity_url;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@JsonProperty("id")
	public Integer getId() {
	return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
	this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
	return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
	this.name = name;
	}
}
