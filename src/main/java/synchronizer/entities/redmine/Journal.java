package synchronizer.entities.redmine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Journal {

@JsonProperty("id")
private Integer id;
@JsonProperty("notes")
private String notes;
@JsonProperty("created_on")
private String createdOn;
@JsonProperty("private_notes")
private Boolean privateNotes;
@JsonProperty("author")
private Author author;
@JsonProperty("details")
private List<Detail> details = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("id")
public Integer getId() {
return id;
}

@JsonProperty("id")
public void setId(Integer id) {
this.id = id;
}

@JsonProperty("notes")
public String getNotes() {
return notes;
}

@JsonProperty("notes")
public void setNotes(String notes) {
this.notes = notes;
}

@JsonProperty("created_on")
public String getCreatedOn() {
return createdOn;
}

@JsonProperty("created_on")
public void setCreatedOn(String createdOn) {
this.createdOn = createdOn;
}

@JsonProperty("private_notes")
public Boolean getPrivateNotes() {
return privateNotes;
}

@JsonProperty("private_notes")
public void setPrivateNotes(Boolean privateNotes) {
this.privateNotes = privateNotes;
}

@JsonProperty("author")
public Author getAuthor() {
return author;
}

@JsonProperty("author")
public void setAuthor(Author author) {
this.author = author;
}

@JsonProperty("details")
public List<Detail> getDetails() {
return details;
}

@JsonProperty("details")
public void setDetails(List<Detail> details) {
this.details = details;
}

}