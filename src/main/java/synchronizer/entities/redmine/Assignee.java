package synchronizer.entities.redmine;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Assignee {
	@JsonProperty("icon_url")
	private String iconUrl;
	@JsonProperty("identity_url")
	private Object identityUrl;
	@JsonProperty("lastname")
	private String lastname;
	@JsonProperty("firstname")
	private String firstname;
	@JsonProperty("mail")
	private String mail;
	@JsonProperty("login")
	private String login;
	@JsonProperty("id")
	private Integer id;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("icon_url")
	public String getIconUrl() {
	return iconUrl;
	}

	@JsonProperty("icon_url")
	public void setIconUrl(String iconUrl) {
	this.iconUrl = iconUrl;
	}

	@JsonProperty("identity_url")
	public Object getIdentityUrl() {
	return identityUrl;
	}

	@JsonProperty("identity_url")
	public void setIdentityUrl(Object identityUrl) {
	this.identityUrl = identityUrl;
	}

	@JsonProperty("lastname")
	public String getLastname() {
	return lastname;
	}

	@JsonProperty("lastname")
	public void setLastname(String lastname) {
	this.lastname = lastname;
	}

	@JsonProperty("firstname")
	public String getFirstname() {
	return firstname;
	}

	@JsonProperty("firstname")
	public void setFirstname(String firstname) {
	this.firstname = firstname;
	}

	@JsonProperty("mail")
	public String getMail() {
	return mail;
	}

	@JsonProperty("mail")
	public void setMail(String mail) {
	this.mail = mail;
	}

	@JsonProperty("login")
	public String getLogin() {
	return login;
	}

	@JsonProperty("login")
	public void setLogin(String login) {
	this.login = login;
	}

	@JsonProperty("id")
	public Integer getId() {
	return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
	this.id = id;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	}

	}
