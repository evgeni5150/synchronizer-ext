package synchronizer.entities.redmine;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

@JsonTypeName("payload")
@JsonTypeInfo(include = As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class RedminePayload {

	@JsonProperty("action")
	private String action;
	@JsonProperty("url")
	private String url;
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@JsonProperty("issue")
	private RedmineIssue issue;
	@JsonProperty("journal")
	private Journal journal;

	public Journal getJournal() {
		return journal;
	}

	public void setJournal(Journal journal) {
		this.journal = journal;
	}

	public RedmineIssue getIssue() {
		return issue;
	}

	public void setIssue(RedmineIssue issue) {
		this.issue = issue;
	}

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
