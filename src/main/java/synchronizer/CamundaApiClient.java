package synchronizer;

import java.net.URI;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;


@Component
public class CamundaApiClient
{
  @Value("${CAMUNDA_URL}")
  private String CAMUNDA_URL;


  public URI generateCamundaTaskCompleteUrl(String taskId)
  {
    String url = CAMUNDA_URL.endsWith("/") ? StringUtils.chop(CAMUNDA_URL) : CAMUNDA_URL;
    url = MessageFormat.format("{0}/external-task/{1}/complete", url, taskId);
    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
    return builder.build().encode().toUri();
  }


  public void completeExternalTask(String taskId, String workerId, String status) throws JsonProcessingException, InterruptedException
  {
    // Send the callback to the process engine.
    URI url = generateCamundaTaskCompleteUrl(taskId);
    Map<String, String> variables = new HashMap<String, String>();
    variables.put("status", status);
    String body = generateBpmnComplete(workerId, variables);
   // String json = MessageFormat.format("'{'\"workerId\":\"{0}\"'}'", workerId);
    RestClient.postJson(body, url);
  }

	private static <T> String generateBpmnComplete(String workerId, Map<String, T> variables) {
		JSONObject vars = new JSONObject();
		for (Map.Entry<String, T> v : variables.entrySet())
		{
		JSONObject var = new JSONObject();
		var.put("value", v.getValue());
		vars.put(v.getKey(), var);
		}
		JSONObject body = new JSONObject();
		body.put("workerId", workerId);
		body.put("variables", vars);
		return body.toString();
	}
}
