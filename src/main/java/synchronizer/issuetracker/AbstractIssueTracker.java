package synchronizer.issuetracker;

import java.io.IOException;
import java.net.URI;
import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.client.task.ExternalTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import synchronizer.Issue;
import synchronizer.IssueMapper;
import synchronizer.RestClient;
import synchronizer.entities.BpmnTask;
import synchronizer.entities.redmine.RedmineIssueResponse;


public abstract class AbstractIssueTracker implements IssueTracker
{
  private static final Logger logger = LoggerFactory.getLogger(AbstractIssueTracker.class);

  private URI uri;


  @Override
  public Issue createBpmnTaskIssue(ExternalTask task) throws RestClientException, InterruptedException, IOException
  {
    String activityId = task.getActivityId();
    Issue issue;
    String taskJSON = "";
    try
    {
      taskJSON = serializeTask(task);
    }
    catch(JsonProcessingException ex)
    {
      ex.printStackTrace();
      throw ex;
    }

    // send newly created issue to the Issue Tracker
    try
    {
      String result = RestClient.postJson(taskJSON, getUri());
      ObjectMapper mapper = new ObjectMapper();
      issue = mapper.readValue(result, RedmineIssueResponse.class);
    }
    catch(RestClientException ex)
    {
      logger.warn(MessageFormat.format("Cannot create issue for task with id: {0}. Issue tracker is unreachable.", activityId));
      // TODO enable notification service and setup backup procedure
      // taskRepository.save(task); TODO store the task only when all attempts failed
      throw ex;
    }
    return issue;
  }


  protected URI getUri()
  {
    if(uri == null)
    {
      uri = createUri();
    }
    return uri;
  }


  protected URI createUri()
  {
    String url = getBaseUrl();
    String apiKey = getApiKey();
    String path = getApiPath();
    url = url.endsWith("/") ? StringUtils.chop(url) : url;
    url = url + path;
    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
    builder = apiKey.length() > 0 ? builder.queryParam("key", apiKey) : builder;
    return builder.build().encode().toUri();
  }


  protected abstract String getBaseUrl();


  protected abstract String getApiPath();


  protected abstract String getApiKey();


  protected abstract Issue createNewIssueInstance();


  protected String serializeTask(ExternalTask task) throws JsonProcessingException
  {
    BpmnTask taskIssue = BpmnTask.fromExternalTask(task);
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(taskIssue);
  }


}
