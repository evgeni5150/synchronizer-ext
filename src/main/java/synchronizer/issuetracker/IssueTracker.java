package synchronizer.issuetracker;

import java.io.IOException;

import org.camunda.bpm.client.task.ExternalTask;

import synchronizer.Issue;
import synchronizer.IssueTrackers;


public interface IssueTracker
{
  public Issue createBpmnTaskIssue(ExternalTask task) throws InterruptedException, IOException;


  public IssueTrackers getType();


  public static IssueTracker get(IssueTrackers t)
  {
    switch(t)
    {
      case REDMINE:
      {
        return new RedmineIssueTracker();
      }
      default:
      {
        return new JiraIssueTracker();
      }
    }
  }
}
