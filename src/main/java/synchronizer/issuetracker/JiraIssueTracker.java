package synchronizer.issuetracker;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import synchronizer.Issue;
import synchronizer.IssueTrackers;
import synchronizer.entities.jira.JiraIssue;


@Component
public class JiraIssueTracker extends AbstractIssueTracker implements IssueTracker
{

  @Value("${JIRA_URL}")
  private String URL;

  @Value("${JIRA_API_KEY}")
  private String API_KEY;

  @Value("${JIRA_API_PATH}")
  private String API_PATH;


  @Override
  public IssueTrackers getType()
  {
    return IssueTrackers.JIRA;
  }


  @Override
  protected Issue createNewIssueInstance()
  {
    return new JiraIssue();
  }


  @Override
  protected String getBaseUrl()
  {
    return URL;
  }


  @Override
  protected String getApiPath()
  {
    return API_PATH;
  }


  @Override
  protected String getApiKey()
  {
    return API_KEY;
  }


}
