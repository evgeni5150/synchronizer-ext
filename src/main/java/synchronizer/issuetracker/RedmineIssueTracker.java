package synchronizer.issuetracker;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import synchronizer.Issue;
import synchronizer.IssueTrackers;
import synchronizer.entities.redmine.RedmineIssue;


@Component
public class RedmineIssueTracker extends AbstractIssueTracker implements IssueTracker
{

  @Value("${REDMINE_URL}")
  private String URL;

  @Value("${REDMINE_API_KEY}")
  private String API_KEY;

  @Value("${REDMINE_API_PATH}")
  private String API_PATH;


  @Override
  protected Issue createNewIssueInstance()
  {
    return new RedmineIssue();
  }


  @Override
  public IssueTrackers getType()
  {
    return IssueTrackers.REDMINE;
  }


  @Override
  protected String getBaseUrl()
  {
    return URL;
  }


  @Override
  protected String getApiPath()
  {
    return API_PATH;
  }


  @Override
  protected String getApiKey()
  {
    return API_KEY;
  }


}
