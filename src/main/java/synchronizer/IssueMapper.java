package synchronizer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import synchronizer.entities.jira.JiraIssue;
import synchronizer.entities.redmine.RedmineIssue;
import synchronizer.entities.redmine.RedminePayload;


public class IssueMapper extends ObjectMapper
{

  private static final long serialVersionUID = 1L;


  public Issue readValue(String content) throws JsonParseException, JsonMappingException, IOException
  {
    // TODO: Parse JSON and decide tracker type and class based on content.
    IssueTrackers trackerType = IssueTrackers.REDMINE;

    switch(trackerType)
    {
      case REDMINE:
      {
        RedminePayload pl =  super.readValue(content, RedminePayload.class);
        return pl.getIssue();
      }
      default:
      {
        return super.readValue(content, JiraIssue.class);
      }
    }
  }
}
