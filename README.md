# Redmine Integration for BPMN (Camunda)

Custom asynchronous service tasks that synchronizes Camunda BPMN Engine with Redmine issue tracker. 

## Getting Started

### Prerequisites


 * Redmine
 * MongoDB
 * Camunda Modeler
 * Docker
 * Synchronizer Modeler Plugin
 * [Redmine WebHook Plugin](https://github.com/suer/redmine_webhook)


### Run

The project uses Spring Boot Starter that comes with embedded Tomcat container. Run the WebService class as Java Application and the project will be up and running on port 8090. 

### Running with Docker

You can also run the application directly with Docker.

Build with maven: ``mvn install dockerfile:build``

Run it: `` docker run -p 8090:8090 --net="host" -t isoft/synchronizer ``

### Features

This project makes it easy to:

 * Create custom tasks that are synchronised with Redmine and Jira
 * Make sure you don't loose any data in the process

It comes with: 

 * Custom service task template for Camunda Modeler. 
 * Java based web service
  
    
### How it works?

The custom service task template can be used by the Camunda modeler. If you already installed the Synchronizer Modeler Plugin and synchronized your Modeler with your Issue Tracker, you can create a Service Task and the Element Template dropdown will appear in the Service Task's Properties Panel. Custom template's name is "Synchronized". Choose "Synchronized" from the dropdown and a couple of custom fields will appear in the panel. This is the place to define the issue specific fields like subject and description. You can leave some of them blank or all of them blank, and generate them as process variables during the execution. 

![Process diagram](src/main/resources/process.png?raw=true)

Deploy your process and start instance of it. When the process execution reaches a task that is defined as "Synchronized" it subscribes to a Topic named "synchronizedTask". The Web Service is polling for tasks subscribed for that topic, so it fetches and locks the task. The Web Service takes care of the communication with the issue tracker's REST API while keeping the data in its own database during that time so you can't loose any data if the issue tracker is unreachable. When the issue is created in the issue tracker, it is configured to send a callback to the Web Service on status change using webhook. 

When the issue status is changed, Issue Tracker sends post request to the Web Service. The Web Service checks the status of the issue and signals the bpmn process. 

If the Issue Tracker is unreachable at the moment of request, the data is stored to the database and a retry mechanism is activated. If it fails, the Web Service notifies the people responsible via E-mail and Telegram. Optionally you can enable a Quartz Scheduler job for more complex retry procedure.

## Built With

* [Spring Boot](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/htmlsingle/) - The java framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [MongoDB](https://docs.mongodb.com/) - NoSQL database to store references, jobs and triggers 
* [Quartz](http://www.quartz-scheduler.org/documentation/) - Used as retry mechanism for post requests to the issue tracker